"use strict";

window.onload = function() {

	/* isBigger */
	var isBigger = function(a, b) {

		return a > b;
	};

	/* isSmaller */
	var isSmaller = function(a, b) {

		return a < b;
	};

	/* getMin */
	var getMin = function() {
		var i = 0;
		var length = arguments.length;
		var result = arguments[0];

		for (; i < length; i++) {
			if (result > arguments[i]) {
				result = arguments[i];
			}
		}

		return result;
	};

	/* pow */
	var pow = function(a, b) {
		var i = 1;
		var result = a;

		for (; i < b; i++) {
			result *= a;
		}
		
		return result;
	};

	/* getClosestToZero */
	var getClosestToZero = function() {
		var i = 0;
		var length = arguments.length;
		var arr = [];
		var arrPrint = [];
		var index = 0;

		for (; i < length; i++) {
			arrPrint.push(arguments[i]);
			arr.push( Math.abs(arguments[i]) );
		}

		index = getIndexMinNumber(arr);

		return arrPrint[index];
	};

	/* getIndexMinNumber */
	function getIndexMinNumber(arr) {
		var i = 0;
		var length = arr.length;
		var number = arr[0];
		var index = 0;
		
		for (; i < length; i++) {
			if (number > arr[i]) {
				number = arr[i];
				index = i;
			}
		}

		return index;
	}

	var page = document.querySelector('.page');

	page.addEventListener('click', function (event) {

		var elemId = event.target.id;

		if (elemId === 'isBigger') {
			console.log( isBigger(5, -1) );
		}

		if (elemId === 'isSmaller') {
			console.log( isSmaller(5, -1) );
		}

		if (elemId === 'getMin') {
			console.log( getMin(3, 0, -3) );
		}

		if (elemId === 'pow') {
			console.log( pow(7, 2) );
		}
		
		if (elemId === 'getClosestToZero') {
			console.log( getClosestToZero(9, 5, -4, -9) );
		}
	});
};