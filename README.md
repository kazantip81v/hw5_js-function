1.	isBigger - a function which accepts two arguments and returns true if first one has greater value than second one or false otherwise. 
E.g: isBigger(5, -1); // => true
Tip: no need for if/else clause nor ternary operator


2.	isSmaller - a function which accepts two arguments and returns true if first one has lesser value than second one or false otherwise.
E.g. isSmaller(5, -1); // => false
Tip: consider reusing isBigger function
	
3.	getMin - a function which accepts arbitrary number of integer arguments and returns the one with the smallest value.
E.g. getMin(3, 0, -3); // => -3
Tip: since arguments is like array, you can use simple iteration over it
and use arguments[ i ] to get the argument of a given index
 
4.	pow(x, n) - Write the function pow (x, n), which returns x to the power of n.
 
5.	getClosestToZero - a function which accepts arbitrary number of integer arguments and returns one closest to 0 (zero).
E.g. getClosestToZero(9, 5, -4, -9); // => -4
	Tip: Math.abs() might be helpful


6.	BONUS, means this task is not obligatory.
guessingGame - rewrite guess a number game using functions.
The main idea is to refactor your previous code so it become reusable and more readable. And task 1,2.